(function() {

  if (typeof Hanoi == "undefined") {
    window.Hanoi = {};
  }

  var GameView = Hanoi.GameView = function(game, $el) {
    this.game = game;
    this.$el = $el;
    this.SIZE = 3;
    this.makeTowers();
    this.bindListeners();
    this.makeFirstTower();
    // this.fromIndex = null;
  };

  GameView.prototype.makeTowers = function() {

    var $game = this.$el.find("#game");
    for (var i = 0; i < 3; i++) {
      var $tower = $("<ul class=\"tower\"></ul>");

      for (var j = 0; j < this.SIZE; j++) {
        $tower.append($("<li></li>"));
      }
      $game.append($tower);
    }

  }


  GameView.prototype.bindListeners = function () {
    var $towers = this.$el.find("#game").find(".tower");
    this.$el.find(".tower").on ("click", function (event) {
      var index = $towers.index(event.currentTarget);
      if (this.fromIndex != null) {
        this.moveDisk(this.fromIndex, index);
        this.fromIndex = null;
      } else {
        this.fromIndex = index;
      }
    }.bind(this));
  }

  GameView.prototype.makeFirstTower = function() {
    var leftTower = this.$el.find("#game").find(".tower")[0];
    var $leftTower = $(leftTower);
    var $towerRings = $leftTower.find("li");


    var c = ["one", "two", "three"];
    for (var i = 0; i < this.SIZE; i++) {
      var $towerRing = $($towerRings[i]);
      $towerRing.addClass("ring-width-"+c[i]);
    }
  }

  GameView.prototype.changeDisplay = function(start, end) {
    var $startTower = $(this.$el.find("#game").find(".tower")[start]);
    var $endTower = $(this.$el.find("#game").find(".tower")[end]);
    var $rings = $startTower.find("li");
    var $endRings = $endTower.find("li");

    var ringClass = "";
    $rings.each(function(index) {
      $ring = $(this);
      if (!ringClass) {
        ringClass = $ring.attr("class");
        $ring.toggleClass(ringClass);
      }
    })

    var endIndex = null;
    for (var i = 0; i < this.SIZE; i++) {
      var $ring = $($endRings[i]);
      var cRingClass = $ring.attr("class");
      if (cRingClass) {
        endIndex = i-1;
        break;
      }
    }
    if (endIndex === null) {
      endIndex = this.SIZE - 1;
    }

    $ring = $($endRings[endIndex]);
    $ring.addClass(ringClass);
  }

  GameView.prototype.moveDisk = function(start, end) {
    if (this.game.isWon()) {
      return;
    }
    if(this.game.move(start, end)) {
      this.changeDisplay(start,end);
    }
    else {
      console.log("Error");
    }
    if (this.game.isWon()) {
      this.$el.find(".winner").text("You win");
      return;
    }
  };

})();
